import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import {Observable } from "rxjs";

import { AuthService } from './auth.service';

const TOKEN_HEADER_KEY = 'Authorization'

@Injectable({
  providedIn: 'root'
})
export class ResumeService {

  API_URL: string = 'http://localhost:5000/api'

  constructor(private httpclient: HttpClient, public auth: AuthService) { }

  //Personal Details
  addPersonalDetails(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/personalDetails/add/` + id, data, {headers: header})
  }

  listPersonalDetailsByUser(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/personalDetails/list/` + id, {headers: header})
  }

  getPersonalDetailByID(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/personalDetails/` + id, {headers: header})
  }

  updatePersonalDetails(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/personalDetails/update/` + id, data, {headers: header})
  }

  deletePersonalDetails(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/personalDetails/delete/` + id)
  }

  //Summary
  addSummary(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/summary/add/` + id, data, {headers: header})
  }

  listSummaryByUser(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/summary/list/` + id, {headers: header})
  }

  getSummaryByID(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/summary/` + id, {headers: header})
  }

  updateSummary(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/summary/update/` + id, data, {headers: header})
  }

  deleteSummary(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/summary/delete/` + id)
  }

  //Employment
  addEmployment(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/employment/add/` + id, data, {headers: header})
  }

  listEmployment(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/employment/list/` + id, {headers: header})
  }

  getEmploymentByID(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/employment/` + id, {headers: header})
  }

  updateEmployment(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/employment/update/` + id, data, {headers: header})
  }

  deleteEmployment(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/employment/delete/` + id)
  }

  //Education
  addEducation(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/education/add/` + id, data, {headers: header})
  }

  listEducation(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/education/list/` + id, {headers: header})
  }

  getEducationByID(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/education/` + id, {headers: header})
  }

  updateEducation(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/education/update/` + id, data, {headers: header})
  }

  deleteEducation(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/education/delete/` + id)
  }

  //Skills
  addSkill(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/skill/add/` + id, data, {headers: header})
  }

  listSkill(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/skill/list/` + id, {headers: header})
  }

  getSkillByID(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/skill/` + id, {headers: header})
  }

  updateSkill(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/skill/update/` + id, data, {headers: header})
  }

  deleteSkill(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/skill/delete/` + id)
  }

  //Languages
  addLanguage(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/language/add/` + id, data, {headers: header})
  }

  listLanguage(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/language/list/` + id, {headers: header})
  }

  getLanguageByID(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/language/` + id, {headers: header})
  }

  updateLanguage(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/language/update/` + id, data, {headers: header})
  }

  deleteLanguage(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/language/delete/` + id)
  }

  //References
  addReference(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/reference/add/` + id, data, {headers: header})
  }

  listReference(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/reference/list/` + id, {headers: header})
  }

  getReferenceByID(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/reference/` + id, {headers: header})
  }

  updateReference(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/reference/update/` + id, data, {headers: header})
  }

  deleteReference(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/reference/delete/` + id)
  }

}
