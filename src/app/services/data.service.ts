import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import {Observable } from "rxjs";

import { AuthService } from './auth.service';

const TOKEN_HEADER_KEY = 'Authorization'

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  API_URL: string = 'http://localhost:5000/api'

  constructor(private httpclient: HttpClient, public auth: AuthService) { }

  register(data): Observable<any> {
    return this.httpclient.post(`${this.API_URL}/users/register`, data)
  }

  login(data): Observable<any> {
    return this.httpclient.post(`${this.API_URL}/users/login`, data)
  }

  getCurrentUser(): Observable<any> {
    return this.httpclient.get(`${this.API_URL}/users/profile`)
  }

  getAllUsers(): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.get(`${this.API_URL}/users/userlist`, {headers: header})
  }

  getProfile(): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.get(`${this.API_URL}/users/profile`, {headers: header})
  }

  getUserById(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.get(`${this.API_URL}/users/user/` + id, {headers: header})
  }

  updateInfo(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.put(`${this.API_URL}/users/update/` + id, data, {headers: header})
  }

  deleteUserById(id): Observable<any> {
    return this.httpclient.delete(`${this.API_URL}/users/delete/` + id)
  }

  addTask(id, data): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    );
    return this.httpclient.post(`${this.API_URL}/task/addtask/` + id, data, {headers: header})
  }

  listTask(): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/task/task-list/`, {headers: header})
  }

  listTaskByUser(id): Observable<any> {
    let header = new HttpHeaders().set(
      "Authorization",
       localStorage.getItem("token")
    )
    return this.httpclient.get(`${this.API_URL}/task/task-list/` + id, {headers: header})
  }
  

}
