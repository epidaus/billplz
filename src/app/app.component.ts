import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from "rxjs/internal/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'billplz';
  currURL: any

  constructor(public router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.router.events.pipe(
      filter((e): e is NavigationEnd => e instanceof NavigationEnd)
    ).subscribe((res) => 
    
      // console.log(res.url)
      this.currURL = res.url
      
      )
    
  }
}
