import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService as AuthGuard } from './services/auth-guard.service';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UsersComponent } from './components/users/users.component';
import { ProfileEditComponent } from './components/home/profile-edit/profile-edit.component';
import { ProfileComponent } from './components/home/profile/profile.component';
import { UserDetailsComponent } from './components/users/user-details/user-details.component';
import { UserEditComponent } from './components/users/user-edit/user-edit.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { TaskListComponent } from './components/tasks/task-list/task-list.component'
import { TaskEditComponent } from './components/tasks/task-edit/task-edit.component'
import { UserAddComponent } from './components/users/user-add/user-add.component'
import { TaskAddComponent } from './components/tasks/task-add/task-add.component'
import { ResumeComponent } from './components/resume/resume.component'
import { ViewResumeComponent } from './components/resume/view-resume/view-resume.component'
import { AddSectionComponent } from './components/resume/add-section/add-section.component'
import { EditSectionComponent } from './components/resume/edit-section/edit-section.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full',
        canActivate: [AuthGuard]
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'edit/:id',
        component: ProfileEditComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'resumeDashboard',
    component: ResumeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'add-section/:section',
        component: AddSectionComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'edit/:section/:id',
        component: EditSectionComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'myResume',
    component: ViewResumeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
      component: UsersComponent,
      canActivate: [AuthGuard],
      children: [
        {
          path: '',
          redirectTo: 'user-details',
          pathMatch: 'full',
          canActivate: [AuthGuard]
        },
        {
          path: 'user-details',
          component: UserDetailsComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'user-edit/:id',
          component: UserEditComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'user-add',
          component: UserAddComponent,
          canActivate: [AuthGuard]
        }
      ]
  },
  {
    path: 'tasks',
    component: TasksComponent,
    children: [
      {
        path: '',
        redirectTo: 'task-list',
        pathMatch: 'full',
        canActivate: [AuthGuard]
      },
      {
        path: 'task-list',
        component: TaskListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'task-edit/:id',
        component: TaskEditComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'task-add',
        component: TaskAddComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
