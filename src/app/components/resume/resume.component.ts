import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { MatDialog  } from '@angular/material/dialog';

import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { DataService } from '../../services/data.service'
import { ResumeService } from '../../services/resume.service'

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit {

  currUser: any

  personalDetails: any

  summary: any

  employment: any

  educations: any
  eduId: any

  skills: any

  languages: any

  references: any

  constructor(public router: Router, private data: DataService, private resumeData: ResumeService, private spinner: NgxSpinnerService, public dialog: MatDialog) { }

  ngOnInit(): void {

    this.data.getProfile().subscribe(res => {
      this.currUser = res.user

      //PersonalDetails
      this.resumeData.listPersonalDetailsByUser(this.currUser._id).subscribe(res => {
        this.personalDetails = res
        console.log(this.personalDetails);
        
      })

      //Summary
      this.resumeData.listSummaryByUser(this.currUser._id).subscribe(res => {
        this.summary = res
      })

      //EmploymentHistory
      this.resumeData.listEmployment(this.currUser._id).subscribe(res => {
        this.employment = res
      })

      //EducationBackground
      this.resumeData.listEducation(this.currUser._id).subscribe(res => {
        this.educations = res
      })

      //Skills
      this.resumeData.listSkill(this.currUser._id).subscribe(res => {
        this.skills = res
      })

      //Languages
      this.resumeData.listLanguage(this.currUser._id).subscribe(res => {
        this.languages = res
      })

      //References
      this.resumeData.listReference(this.currUser._id).subscribe(res => {
        this.references = res
      })
      // this.refresh()
    })
    
  }

  refresh() {
    this.ngOnInit()
  }

  delete(id, section) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this Item?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        if(section == 'personalDetails') {
          this.resumeData.deletePersonalDetails(id).subscribe(res => {
            if(res) {
              this.refresh()
            }
          })
        } else if (section == 'summary') {
          this.resumeData.deleteSummary(id).subscribe(res => {
            if(res) {
              this.refresh()
            }
          })
        } else if (section == 'employment') {
          this.resumeData.deleteEmployment(id).subscribe(res => {
            if(res) {
              this.refresh()
            }
          })
        } else if (section == 'education') {
          this.resumeData.deleteEducation(id).subscribe(res => {
            if(res) {
              this.refresh()
            }
          })
        } else if (section == 'skill') {
          this.resumeData.deleteSkill(id).subscribe(res => {
            if(res) {
              this.refresh()
            }
          })
        } else if (section == 'language') {
          this.resumeData.deleteLanguage(id).subscribe(res => {
            if(res) {
              this.refresh()
            }
          })
        } else if (section == 'reference') {
          this.resumeData.deleteReference(id).subscribe(res => {
            if(res) {
              this.refresh()
            }
          })
        } else {}
          
      }
    });

    
  }

  add(section) {
    this.router.navigate(['/resumeDashboard/add-section/' + section])
  }

  edit(section, id) {
    this.router.navigate(['/resumeDashboard/edit/' + section + '/' + id])
  }

}
