import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DataService } from '../../../services/data.service';
import { ResumeService } from '../../../services/resume.service'

@Component({
  selector: 'app-view-resume',
  templateUrl: './view-resume.component.html',
  styleUrls: ['./view-resume.component.scss']
})
export class ViewResumeComponent implements OnInit {

  currUser: any
  userID: any
  datas: any
  datam: any
  personaldetails: any
  summary:any
  educations: any
  employments: any
  skills: any
  languages: any
  references: any

  constructor(private resumeData: ResumeService, private data: DataService, public router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.data.getProfile().subscribe(res => {
      this.currUser = res
      this.userID = this.currUser.user._id

      this.resumeData.listPersonalDetailsByUser(this.userID).subscribe(res => {
        this.datas = res
        this.datam = Object.assign({}, this.datas)
        this.personaldetails = this.datam[0]
      })

      this.resumeData.listSummaryByUser(this.userID).subscribe(res => {
        this.datas = res
        this.datam = Object.assign({}, this.datas)
        this.summary = this.datam[0]
      })

      this.resumeData.listEducation(this.userID).subscribe(res => {
        this.educations = res
      })

      this.resumeData.listEmployment(this.userID).subscribe(res => {
        this.employments = res
      })

      this.resumeData.listSkill(this.userID).subscribe(res => {
        this.skills = res
      })

      this.resumeData.listLanguage(this.userID).subscribe(res => {
        this.languages = res
      })

      this.resumeData.listReference(this.userID).subscribe(res => {
        this.references = res
      })
      
    })
  }

}
