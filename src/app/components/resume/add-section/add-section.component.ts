import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { DataService } from '../../../services/data.service';
import { ResumeService } from '../../../services/resume.service'

@Component({
  selector: 'app-add-section',
  templateUrl: './add-section.component.html',
  styleUrls: ['./add-section.component.scss']
})
export class AddSectionComponent implements OnInit {

  skor = [
    {
      title: 'Select',
      value: null
    },
    {
      title: 'Novice',
      value: 1
    },
    {
      title: 'Beginner',
      value: 2
    },
    {
      title: 'Skillful',
      value: 3
    },
    {
      title: 'Experienced',
      value: 4
    },
    {
      title: 'Expert',
      value: 5
    }
  ]

  currSection: any
  currUser: any

  constructor(private resumeData: ResumeService, private data: DataService, private fb: FormBuilder, public router: Router, private activatedRoute: ActivatedRoute) { }

  personalForm = this.fb.group({
    job_title: ['', Validators.required],
    fname: ['', Validators.required],
    lname: ['', Validators.required],
    email: ['', Validators.required],
    phone: ['', Validators.required]
  })

  summaryForm = this.fb.group({
    summary_desc: ['', Validators.required]
  })

  employmentForm = this.fb.group({
    startdate: ['', Validators.required],
    enddate: ['', Validators.required],
    title: ['', Validators.required],
    company_name: ['', Validators.required]
  })

  educationForm = this.fb.group({
    startdate: ['', Validators.required],
    enddate: ['', Validators.required],
    title: ['', Validators.required],
    school: ['', Validators.required],
    result: ['', Validators.required]
  })

  skillForm = this.fb.group({
    title: ['', Validators.required],
    score: ['', Validators.required]
  })

  langugaeForm = this.fb.group({
    title: ['', Validators.required],
    score: ['', Validators.required]
  })

  referenceForm = this.fb.group({
    name: ['', Validators.required],
    title: ['', Validators.required],
    company: ['', Validators.required],
    email: ['', Validators.required],
    phone: ['', Validators.required]
  })

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(({ section }) => {
      this.currSection = section
      
    })
  }

  add(section) {

    this.data.getProfile().subscribe(res => {
      this.currUser = res.user
      
      if(section == 'personalDetails') {
        this.resumeData.addPersonalDetails(this.currUser._id, this.personalForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'summary') {
        this.resumeData.addSummary(this.currUser._id, this.summaryForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'employment') {
        this.resumeData.addEmployment(this.currUser._id, this.employmentForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'education') {
        this.resumeData.addEducation(this.currUser._id, this.educationForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'skill') {
        this.resumeData.addSkill(this.currUser._id, this.skillForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'language') {
        this.resumeData.addLanguage(this.currUser._id, this.langugaeForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'reference') {
        this.resumeData.addReference(this.currUser._id, this.referenceForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else {}
      
    })
    
  }

  back() {
    this.router.navigate(['resumeDashboard'])
  }

}
