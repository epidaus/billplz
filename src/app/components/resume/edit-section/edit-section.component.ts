import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { DataService } from '../../../services/data.service';
import { ResumeService } from '../../../services/resume.service'

@Component({
  selector: 'app-edit-section',
  templateUrl: './edit-section.component.html',
  styleUrls: ['./edit-section.component.scss']
})
export class EditSectionComponent implements OnInit {

  skor = [
    {
      title: 'Select',
      value: null
    },
    {
      title: 'Novice',
      value: 1
    },
    {
      title: 'Beginner',
      value: 2
    },
    {
      title: 'Skillful',
      value: 3
    },
    {
      title: 'Experienced',
      value: 4
    },
    {
      title: 'Expert',
      value: 5
    }
  ]

  currSection: any
  currUser: any
  datas: any

  constructor(private resumeData: ResumeService, private data: DataService, private fb: FormBuilder, public router: Router, private activatedRoute: ActivatedRoute) { }

  personalForm = this.fb.group({
    job_title: ['', Validators.required],
    fname: ['', Validators.required],
    lname: ['', Validators.required],
    email: ['', Validators.required],
    phone: ['', Validators.required]
  })

  summaryForm = this.fb.group({
    summary_desc: ['', Validators.required]
  })

  employmentForm = this.fb.group({
    startdate: ['', Validators.required],
    enddate: ['', Validators.required],
    title: ['', Validators.required],
    company_name: ['', Validators.required]
  })

  educationForm = this.fb.group({
    startdate: ['', Validators.required],
    enddate: ['', Validators.required],
    title: ['', Validators.required],
    school: ['', Validators.required],
    result: ['', Validators.required]
  })

  skillForm = this.fb.group({
    title: ['', Validators.required],
    score: ['', Validators.required]
  })

  langugaeForm = this.fb.group({
    title: ['', Validators.required],
    score: ['', Validators.required]
  })

  referenceForm = this.fb.group({
    name: ['', Validators.required],
    title: ['', Validators.required],
    company: ['', Validators.required],
    email: ['', Validators.required],
    phone: ['', Validators.required]
  })

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(({ section }) => {
      this.currSection = section
      if(this.currSection == 'personalDetails') {
        this.activatedRoute.params.subscribe(({ id }) => {
          this.resumeData.getPersonalDetailByID(id).subscribe(res => {
            this.datas = res
          })
        });
      } else if(this.currSection == 'summary') {
        this.activatedRoute.params.subscribe(({ id }) => {
          this.resumeData.getSummaryByID(id).subscribe(res => {
            this.datas = res
          })
        });
      } else if(this.currSection == 'employment') {
        this.activatedRoute.params.subscribe(({ id }) => {
          this.resumeData.getEmploymentByID(id).subscribe(res => {
            this.datas = res
          })
        });
      } else if(this.currSection == 'education') {
        this.activatedRoute.params.subscribe(({ id }) => {
          this.resumeData.getEducationByID(id).subscribe(res => {
            this.datas = res
          })
        });
      } else if(this.currSection == 'skill') {
        this.activatedRoute.params.subscribe(({ id }) => {
          this.resumeData.getSkillByID(id).subscribe(res => {
            this.datas = res
          })
        });
      } else if(this.currSection == 'language') {
        this.activatedRoute.params.subscribe(({ id }) => {
          this.resumeData.getLanguageByID(id).subscribe(res => {
            this.datas = res
          })
        });
      } else if(this.currSection == 'reference') {
        this.activatedRoute.params.subscribe(({ id }) => {
          this.resumeData.getReferenceByID(id).subscribe(res => {
            this.datas = res
          })
        });
      } else {}
      
    })

  }

  save(section, id) {
      
      if(section == 'personalDetails') {
        this.resumeData.updatePersonalDetails(id, this.personalForm.value).subscribe((res) => {
          if (res) {
            // this.router.navigate(['resumeDashboard'])
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'summary') {
        this.resumeData.updateSummary(id, this.summaryForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'employment') {
        this.resumeData.updateEmployment(id, this.employmentForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'education') {
        this.resumeData.updateEducation(id, this.educationForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'skill') {
        this.resumeData.updateSkill(id, this.skillForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'language') {
        this.resumeData.updateLanguage(id, this.langugaeForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else if (section == 'reference') {
        this.resumeData.updateReference(id, this.referenceForm.value).subscribe((res) => {
          if (res) {
            this.router.navigate(['resumeDashboard'])
              .then(() => {
                window.location.reload();
              });
          }
        })
      } else {}
    
  }

  back() {
    this.router.navigate(['resumeDashboard'])
  }

}
