import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from "rxjs/internal/operators";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  tab: any = 'tab1';
  tab1: any
  tab2: any
  tab3: any
  tab4: any
  tab5: any

  constructor(public router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    // this.router.events.pipe(
    //   filter((e): e is NavigationEnd => e instanceof NavigationEnd)
    // ).subscribe((res) => console.log(res.url))
    
  }

  logout() {
    localStorage.removeItem('token')
    this.router.navigate(['login'])
  }

  onClick(check){
        if(check==1){
          this.tab = 'tab1';
        }
        else if(check==2) {
          this.tab = 'tab2';
        }
        else if(check==3) {
          this.tab = 'tab3';
        }
        else if(check==4) {
          this.tab = 'tab4';
        }
        else if(check==5) {
          this.tab = 'tab5';
        }
        else{
          this.tab = 'tab1';
        }    
      
    }


}
