import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { JwtModule, JwtModuleOptions, JwtHelperService } from '@auth0/angular-jwt'
import { HttpClientModule } from '@angular/common/http'
import { NgxSpinnerModule } from 'ngx-spinner'
import { NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap'
import { MatDialogModule } from '@angular/material/dialog'
import { MatButtonModule } from '@angular/material/button'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { HomeComponent } from './components/home/home.component'
import { LoginComponent } from './components/login/login.component'
import { NavbarComponent } from './components/navbar/navbar.component'
import { RegisterComponent } from './components/register/register.component'
import { ControlMessagesComponent } from './components/control-messages/control-messages.component'
import { UsersComponent } from './components/users/users.component'
import { UserDetailsComponent } from './components/users/user-details/user-details.component'
import { ProfileEditComponent } from './components/home/profile-edit/profile-edit.component'
import { ProfileComponent } from './components/home/profile/profile.component'
import { UserEditComponent } from './components/users/user-edit/user-edit.component'
import { TasksComponent } from './components/tasks/tasks.component'
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component'
import { TaskListComponent } from './components/tasks/task-list/task-list.component'
import { TaskEditComponent } from './components/tasks/task-edit/task-edit.component'
import { UserAddComponent } from './components/users/user-add/user-add.component'
import { TaskAddComponent } from './components/tasks/task-add/task-add.component';
import { ResumeComponent } from './components/resume/resume.component';
import { ViewResumeComponent } from './components/resume/view-resume/view-resume.component';
import { AddSectionComponent } from './components/resume/add-section/add-section.component';
import { EditSectionComponent } from './components/resume/edit-section/edit-section.component'


export function tokenGetter() {
  return localStorage.getItem("token");
}

const JWT_Module_Options: JwtModuleOptions = {
  config: {
    tokenGetter: tokenGetter
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    ControlMessagesComponent,
    UsersComponent,
    UserDetailsComponent,
    ProfileEditComponent,
    ProfileComponent,
    UserEditComponent,
    TasksComponent,
    ConfirmationDialogComponent,
    TaskListComponent,
    TaskEditComponent,
    UserAddComponent,
    TaskAddComponent,
    ResumeComponent,
    ViewResumeComponent,
    AddSectionComponent,
    EditSectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    JwtModule.forRoot(JWT_Module_Options),
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    NgbPaginationModule,
    NgbAlertModule,
    MatDialogModule,
    MatButtonModule
  ],
  providers: [JwtHelperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
